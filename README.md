# gitops

Project to POC working with Flux
https://docs.fluxcd.io/en/latest/tutorials/get-started/
1. create gke cluster
2. set cluster role binding
3. install fluxctl - `brew install fluxctl`
4. set flux namespace `export FLUX_FORWARD_NAMESPACE=flux`
5. create namespace `kubectl create ns flux`
6. do install
```
export GLUSER="mmosttler"
fluxctl install \
--git-user=${GLUSER} \
--git-email=${GLUSER}@users.noreply.gitlab.com \
--git-url=git@gitlab.com:${GLUSER}/gitops-flux.git \
--git-path=namespaces,workloads \
--namespace=flux | kubectl apply -f -
``` 
7. Add flux as authenticated deploy key to update the repo.
- get public key that was generated: `fluxctl identity --k8s-fwd-ns flux`
- in gitlab, Settings -> Repository -> Deploy Keys.
  - Title = 'flux operator'
  - Key = paste the generated key.
  - Write access allowed = true
  - Click _Add key_
- NOTE: user must have *Maintainer* permissions on the repo.

Install is complete.  Now test it with a change to the manifests.

1. Commit change to the `--ui-message` in `/workloads/podinfo-dep.yaml'
2. Default sync is 5min so force the sync now, `fluxctl sync --k8s-fwd-ns flux`
3. Confirm the change.
    - do port forward `kubectl -n demo port-forward deployment/podinfo 9898:9898 &`
    - call endpoint `curl localhost:9898`
